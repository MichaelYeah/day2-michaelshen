Objective:
1.	Tasking at first before developing a software, including splitting complex problems into simple ones, making divisions exclusive and let input and output verifiable.
2.	Try to draw a context map before developing a software, which is useful for us to write code clearer.
3.	Understand the structure, commands and format of Git and Practice to use them in the afternoon.
4.	Practice drawing and coding and communicate with my teammates after that.

Reflective:
1.	I found that I can’t split one problem into simple problems that are detailed enough.
2.	I found that the map I draw are not  detailed enough. For example, I didn’t split one function that contains double loop.
3.	When coding, I didn’t test it as soon as I finish coding one function, and teacher Jaap reminded me. At the same time, I didn’t consider the exceptions. 

Interpretive:
1.	Why I had trouble in tasking and drawing map is that I am not observant and experienced enough.
2.	I didn’t test the code I wrote in time because I haven’t make TDD a habit when I was a university student.

Decisional:
1.	I decide to do more practice in the following projects, especially keep in mind that it’s necessary to tasking and drawing a concept map. Because sharpening your axe will not delay your job of chopping wood.
2.	I need to implement the idea of TDD in my following coding carrer.
