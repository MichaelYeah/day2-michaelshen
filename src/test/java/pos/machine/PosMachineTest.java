package pos.machine;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PosMachineTest {

    @Test
    public void should_return_receipt() {
        PosMachine posMachine = new PosMachine();

        String expected = "***<store earning no money>Receipt***\n" +
                "Name: Coca-Cola, Quantity: 4, Unit price: 3 (yuan), Subtotal: 12 (yuan)\n" +
                "Name: Sprite, Quantity: 2, Unit price: 3 (yuan), Subtotal: 6 (yuan)\n" +
                "Name: Battery, Quantity: 3, Unit price: 2 (yuan), Subtotal: 6 (yuan)\n" +
                "----------------------\n" +
                "Total: 24 (yuan)\n" +
                "**********************";

        assertEquals(expected, posMachine.printReceipt(loadBarcodes()));
    }

    @Test
    public void could_decode_to_items() {
        PosMachine posMachine = new PosMachine();

        List<ReceiptItem> receiptItems = posMachine.decodeToItems(loadBarcodes());

        System.out.println(receiptItems);
    }

    private static List<String> loadBarcodes() {
        return Arrays.asList("ITEM000000", "ITEM000000", "ITEM000000", "ITEM000000", "ITEM000001", "ITEM000001", "ITEM000004", "ITEM000004", "ITEM000004");
    }

    @Test
    public void calculateItemCost() {
        PosMachine posMachine = new PosMachine();

        List<ReceiptItem> receiptItems = posMachine.decodeToItems(loadBarcodes());

        List<ReceiptItem> new_receiptItems = posMachine.calculateItemCost(receiptItems);

        System.out.println(new_receiptItems);
    }

    @Test
    public void calculateTotalPrice() {
        PosMachine posMachine = new PosMachine();

        List<ReceiptItem> receiptItems = posMachine.decodeToItems(loadBarcodes());

        List<ReceiptItem> new_receiptItems = posMachine.calculateItemCost(receiptItems);

        int totalPrice = posMachine.calculateTotalPrice(new_receiptItems);

        System.out.println(totalPrice);
    }

    @Test
    public void calculateCost() {
        PosMachine posMachine = new PosMachine();

        List<ReceiptItem> receiptItems = posMachine.decodeToItems(loadBarcodes());

        Receipt receipt = posMachine.calculateCost(receiptItems);

        System.out.println(receipt);
    }

    @Test
    public void generateItemsReceipt() {
        PosMachine posMachine = new PosMachine();

        List<ReceiptItem> receiptItems = posMachine.decodeToItems(loadBarcodes());

        Receipt receipt = posMachine.calculateCost(receiptItems);

        String itemsReceipt = posMachine.generateItemsReceipt(receipt);

        System.out.println(itemsReceipt);
    }

    @Test
    public void generateReceipt() {
        PosMachine posMachine = new PosMachine();

        List<ReceiptItem> receiptItems = posMachine.decodeToItems(loadBarcodes());

        Receipt receipt = posMachine.calculateCost(receiptItems);

        String itemsReceipt = posMachine.generateItemsReceipt(receipt);

        List<ReceiptItem> new_receiptItems = posMachine.calculateItemCost(receiptItems);

        int totalPrice = posMachine.calculateTotalPrice(new_receiptItems);

        String receipt_str = posMachine.generateReceipt(itemsReceipt, totalPrice);

        System.out.println(receipt_str);
    }

    @Test
    void renderReceipt() {
        PosMachine posMachine = new PosMachine();

        List<ReceiptItem> receiptItems = posMachine.decodeToItems(loadBarcodes());

        Receipt receipt = posMachine.calculateCost(receiptItems);

        String renderReceipt = posMachine.renderReceipt(receipt);

        System.out.println(renderReceipt);
    }
}
