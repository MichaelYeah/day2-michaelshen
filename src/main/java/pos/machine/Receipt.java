package pos.machine;

import java.util.List;

public class Receipt {
    List<ReceiptItem> receiptItems;
    int totalPrice;

    public List<ReceiptItem> getReceiptItems() {
        return receiptItems;
    }

    public void setReceiptItems(List<ReceiptItem> receiptItems) {
        this.receiptItems = receiptItems;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Override
    public String toString() {
        return "{\n" +
                "            receiptItems:\n" + receiptItems.toString() + "\n" +
                "            totalPrice: " + this.totalPrice + "\n" +
                "        }";
    }
}
