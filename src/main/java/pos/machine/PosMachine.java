package pos.machine;

import java.util.*;

import static pos.machine.ItemsLoader.loadAllItems;

public class PosMachine {
    public String printReceipt(List<String> barcodes) {
        List<ReceiptItem> receiptItems = decodeToItems(barcodes);
        Receipt receipt = calculateCost(receiptItems);
        String receipt_str = renderReceipt(receipt);
        return receipt_str;
    }

    public List<ReceiptItem> decodeToItems(List<String> barcodes) {
        ArrayList<ReceiptItem> receiptItems = new ArrayList<>();
        Map<String, Integer> barcodeMap = new HashMap<>();
        ArrayList<String> new_barcodes = new ArrayList<>();
        List<Item> items = loadAllItems();
        for (String barcode: barcodes) {
            if (!barcodeMap.containsKey(barcode)) {
                barcodeMap.put(barcode, 1);
                new_barcodes.add(barcode);
            } else {
                barcodeMap.put(barcode, barcodeMap.get(barcode) + 1);
            }
        }
        for (String barcode: new_barcodes) {
            for (Item item: items) {
                if (item.getBarcode() == barcode) {
                    ReceiptItem receiptItem = new ReceiptItem();
                    receiptItem.setName(item.getName());
                    receiptItem.setQuantity(barcodeMap.get(barcode));
                    receiptItem.setUnitPrice(item.getPrice());
                    receiptItems.add(receiptItem);
                }
            }
        }
        return receiptItems;
    }

    public Receipt calculateCost(List<ReceiptItem> receiptItems) {
        Receipt receipt = new Receipt();
        List<ReceiptItem> receiptItemsAfterCalculateItemCost = calculateItemCost(receiptItems);
        int totalPrice = calculateTotalPrice(receiptItemsAfterCalculateItemCost);
        receipt.setReceiptItems(receiptItemsAfterCalculateItemCost);
        receipt.setTotalPrice(totalPrice);
        return receipt;
    }

    public String renderReceipt(Receipt receipt) {
        String itemsReceipt = generateItemsReceipt(receipt);
        String receipt_str = generateReceipt(itemsReceipt, receipt.getTotalPrice());
        return receipt_str;
    }

    public List<ReceiptItem> calculateItemCost(List<ReceiptItem> receiptItems) {
        List<ReceiptItem> new_receiptItems = new ArrayList<>();
        for (ReceiptItem receiptItem: receiptItems) {
            receiptItem.setSubTotal(receiptItem.getQuantity() * receiptItem.getUnitPrice());
            new_receiptItems.add(receiptItem);
        }
        return new_receiptItems;
    }

    public int calculateTotalPrice(List<ReceiptItem> receiptItems) {
        int totalPrice = 0;
        for (ReceiptItem receiptItem: receiptItems) {
            totalPrice += receiptItem.getSubTotal();
        }
        return totalPrice;
    }

    public String generateItemsReceipt(Receipt receipt) {
        String itemsReceipt = "";
        List<ReceiptItem> receiptItems = receipt.getReceiptItems();
        for (int i = 0; i < receiptItems.size(); i++) {
            ReceiptItem receiptItem = receiptItems.get(i);
            itemsReceipt += "Name: " + receiptItem.getName() + ", Quantity: " + receiptItem.getQuantity() + ", Unit price: " + receiptItem.getUnitPrice() + " (yuan), Subtotal: " + receiptItem.getSubTotal() + " (yuan)";
            if (i != receiptItems.size() - 1) itemsReceipt += "\n";
        }
        return itemsReceipt;
    }

    public String generateReceipt(String itemsReceipt, int totalPrice) {
        return "***<store earning no money>Receipt***\n" + itemsReceipt + "\n----------------------\nTotal: " + totalPrice + " (yuan)\n**********************";
    }

}
